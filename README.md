Creates/deletes a secret from Kubernetes.

The file ``secret.yaml`` has the skeleton of a descriptor,
with more information defined on code. The skeleton is
populated with contents from constants
``k8sNamespace``, ``k8sSecretName`` and ``k8sSecretData``.

You may select if you wish to apply or delete the description
via the constant ``action``.

To run the program, compile it

```
$ npm run compile
````

and start Node:

```
$ npm run start
```