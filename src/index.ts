import * as k8s from '@kubernetes/client-node';
import { KubernetesObject } from '@kubernetes/client-node';
import * as fs from 'fs';
import { promisify } from 'util';
import * as yaml from 'yaml'

const action : "apply" | "delete" | "get" = "get"

const k8sNamespace = 'jiba-test'
const k8sSecretName = 'secret-test'
const k8sSecretData = 
    {  
        user: btoa('my-user'), 
        password: btoa('my-password'),
        otherStuff: btoa('other-stuff')
    }


const kc = new k8s.KubeConfig();
kc.loadFromDefault();
const client = k8s.KubernetesObjectApi.makeApiClient(kc);

main()

async function main()  {
    process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = "0"

    const k8sObject = createK8sObject()

    switch (action) {
        case "apply":
            console.log(await applyK8s(k8sObject))
            return
        case "get":
            console.log(await getK8s(k8sObject))
            return
        case "delete":
            deleteK8s(k8sObject)
    }
}

function createK8sObject(): KubernetesObject {
    const description: {[k: string]: any} = yaml.parse(fs.readFileSync('secret.yaml', 'utf8')) as object

    description.data=k8sSecretData
    description.metadata.name=k8sSecretName
    description.metadata.namespace=k8sNamespace

    return description as KubernetesObject
}


async function deleteK8s(spec: KubernetesObject) {
    client.delete(spec)
}

async function applyK8s(spec: KubernetesObject): Promise<k8s.KubernetesObject> {
    try {
        await getK8s(spec) 
        return client.patch(spec) as KubernetesObject;
    } catch (e) {
        return client.create(spec) as KubernetesObject;
    }
}

async function getK8s(spec: KubernetesObject): Promise<k8s.KubernetesObject> {
    return client.read(spec) as KubernetesObject
}